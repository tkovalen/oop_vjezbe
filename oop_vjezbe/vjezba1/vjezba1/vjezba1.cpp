// vjezba1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>
using namespace std;

int main()
{

	const int N = 10;

	int niz[N];

	for (int i = 0; i < N; i++)
	{
		do {

			cout << "unesi " << i + 1 << ". broj: ";
			cin >> niz[i];

		} while (niz[i] < 1 || niz[i]>9);

	}

		//punimo niz duplikata s nulama
		int duplikati[N];
		for (int i = 0; i < N; i++)
			duplikati[i] = 0;

		cout << "\nniz duplikata: ";

		for (int i = 0; i < N; i++)
		{
			if (duplikati[i] == 0) 
			{
				int brojac = 0;
				for (int j = i; j < N; j++)
				{
					if (niz[j] == niz[i])
					{
						brojac += 1;
						duplikati[j] = 1;
					}
				}
					
				duplikati[i] = brojac;
				cout  <<" "<< duplikati[i];
			}
		}

}
// zadatak3.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
using namespace std;

/*U nizu ícetveroznamenkastih brojeva nalazi se samo jedan broj ícija je suma jedinice i
stotice jednaka 5. Napisite funkciju koja pronalazi taj broj u nizu od N brojeva i vraca
referencu na taj element niza.Koristeci povratnu vrijednost funkcije kao lvalue uvecajte
taj element niza za jedan.Ispisite vrijednost elementa poslije uvecavanja.*/


int& pronadji_broj(int& N)
{

	int prva = N / 1000;
	int treca = (N % 100) / 10;
	if (prva + treca == 5)
		return N;
	else
		N = 0;
}


int main()
{
	int N[7] = { 1435,5467,2457,8865,7654,4353,1243 };



	for (int i = 0; i < 7; i++)	{		if (pronadji_broj(N[i]) != 0)		{			cout << N[i];			pronadji_broj(N[i])++;			cout << "\nvrijednost poslije uvecavanja za 1: " << N[i];		}	}

}

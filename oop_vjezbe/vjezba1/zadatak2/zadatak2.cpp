// zadatak2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <cstdlib>
#include <iostream>
#include <string>
using namespace std;

struct student
{
	string ID;
	char ime[20];
	char spol[20];
	double ocjena_kviz1;
	double ocjena_kviz2;
	double ocjena_midterm;
	double ocjena_final;
	double ukupan_broj_bodova;


}st[20];

void unos(int n)
{
	for (int i = n; i < n+1; i++)
	{
		cout << "\nID: ";
		cin >> st[i].ID;

		//provjera duplikata
		for (int j = 0; j < i; j++)
		{
			if (st[i].ID == st[j].ID)
			{
				do
				{
					cout << "\nID ne smije biti isti! Unesi novi: ";
					cin >> st[i].ID;
				} while (st[i].ID == st[j].ID);

			}
		}

		cout << "\nIme: ";
		cin >> st[i].ime;

		cout << "\nSpol: ";
		cin >> st[i].spol;

		cout << "\nOcjena 1. kviza: ";
		cin >> st[i].ocjena_kviz1;

		cout << "\nOcjena 2. kviza: ";
		cin >> st[i].ocjena_kviz2;

		cout << "\nOcjena na sredini semestra: ";
		cin >> st[i].ocjena_midterm;

		cout << "\nOcjena na kraju semestra: ";
		cin >> st[i].ocjena_final;

		st[i].ukupan_broj_bodova = st[i].ocjena_kviz1 + st[i].ocjena_kviz2 + st[i].ocjena_midterm + st[i].ocjena_final;
	}

}

void ispis(int n)
{
	for (int i = 0; i < n; i++)
	{
		cout << "\nID: " << st[i].ID;
		cout << "\nime: " << st[i].ime;
		cout << "\nspol: " << st[i].spol;
		cout << "\nocjena 1. kviz: " << st[i].ocjena_kviz1;
		cout << "\nocjena 2. kviz: " << st[i].ocjena_kviz2;
		cout << "\nocjena na sredini semestra: " << st[i].ocjena_midterm;
		cout << "\nocjena na kraju semestra: " << st[i].ocjena_final;
		cout << "\nukupan broj bodova: " << st[i].ukupan_broj_bodova;
		cout << "\n";

	}

}




int main()
{
	int n,m,j;
	m = 0;
	n = 2;

	//unos studenta

	for (int i = 0; i < n ; i++)
	{
		cout << "\nID: ";
		cin >> st[i].ID;

		//provjera duplikata
		for (j = 0; j < i; j++)
		{
			if (st[i].ID == st[j].ID)
			{
				do
				{
					cout << "\nID ne smije biti isti! Unesi novi: ";
					cin >> st[i].ID;
				} while (st[i].ID == st[j].ID);
				
			}
		}
		

		cout << "\nIme: ";
		cin >> st[i].ime;

		cout << "\nSpol: ";
		cin >> st[i].spol;

		cout << "\nOcjena 1. kviza: ";
		cin >> st[i].ocjena_kviz1;

		cout << "\nOcjena 2. kviza: ";
		cin >> st[i].ocjena_kviz2;

		cout << "\nOcjena na sredini semestra: ";
		cin >> st[i].ocjena_midterm;

		cout << "\nOcjena na kraju semestra: ";
		cin >> st[i].ocjena_final;

		st[i].ukupan_broj_bodova = st[i].ocjena_kviz1 + st[i].ocjena_kviz2 + st[i].ocjena_midterm + st[i].ocjena_final;
		cout << "\nUkupan broj bodova: " << st[i].ukupan_broj_bodova;
		
	}

	//izbornik

	while (m != 10)
	{
		cout << "\n-----------IZBORNIK-----------\n";

		cout << "\n1.Dodaj novi zapis" << "\n2.Ukloni zapis" << "\n3.Azuriraj zapis" << "\n4.Prikazi sve zapise" << "\n5.Izracunaj prosjek bodova za studenta" << "\n6.Prikazi studenta s najvecim brojem bodova" << "\n7.Prikazi studenta s najmanjim brojem bodova" << "\n8.Pronadji studenta po ID-u" << "\n9.sortiraj zapise po broju bodova(total)" << "\n10.Izlaz\n";

		cin >> m;

		if (m == 1)
		{
			unos(n);
			n++;

		}

		if (m == 2)
		{
			string IDukloni;
			bool flagUkloni = 0;

			cout << "\nUnesi ID studenta kojeg zelis ukloniti: ";
			cin >> IDukloni;

			for (int i = 0; i < n; i++)
			{

				if (IDukloni == st[i].ID)
				{
					flagUkloni = 1;
					for (int j = i; j < (n - 1); j++)
					{
						st[j] = st[j + 1];
					}
					n--;
					break;

				}

			}

			if(flagUkloni==0)
				cout << "\nNe postoji ID studenta!";


		}


		if (m == 3)
		{

			string IDazuriraj;
			bool flagAzuriraj = 0;

			cout << "\nUnesi ID studenta kojeg zelis azurirati: ";
			cin >> IDazuriraj;


			for (int i = 0; i < n; i++)
			{
				if (IDazuriraj == st[i].ID)
				{
					flagAzuriraj = 1;
					cout << "\nIme: ";
					cin >> st[i].ime;

					cout << "\nSpol: ";
					cin >> st[i].spol;

					cout << "\nOcjena 1. kviza: ";
					cin >> st[i].ocjena_kviz1;

					cout << "\nOcjena 2. kviza: ";
					cin >> st[i].ocjena_kviz2;

					cout << "\nOcjena na sredini semestra: ";
					cin >> st[i].ocjena_midterm;

					cout << "\nOcjena na kraju semestra: ";
					cin >> st[i].ocjena_final;

					st[i].ukupan_broj_bodova = st[i].ocjena_kviz1 + st[i].ocjena_kviz2 + st[i].ocjena_midterm + st[i].ocjena_final;
					cout << "\nUkupan broj bodova: " << st[i].ukupan_broj_bodova;
				}

			}

			if (flagAzuriraj == 0)
				cout << "\nNe postoji ID studenta!";

		}
		if (m == 4)
		{
			
			ispis(n);

		}


		if (m == 5)
		{
			string IDprosjek;
			double prosjek = 0;
			bool flagprosjek = 0;

			cout << "\nUnesi ID studenta za kojeg zelis vidjeti prosjek bodova: ";
			cin >> IDprosjek;

			for (int i = 0; i < n; i++)
			{

				if (IDprosjek == st[i].ID)
				{
					flagprosjek = 1;
					prosjek = st[i].ukupan_broj_bodova / 4;
					cout << "\nProsjek bodova: " << prosjek;
				}

			}

			if (flagprosjek == 0)
				cout << "\nID studenta ne postoji!";

		}



		if (m == 6)
		{
			double max = 0;
			student maxst;

			for (int i = 0; i < n; i++)
			{
				if (st[i].ukupan_broj_bodova > max)
				{
					max = st[i].ukupan_broj_bodova;
					maxst = st[i];
				}

			}

			cout << "\nStudent s najvecim brojem bodova je: " << maxst.ime;
		}

		if (m == 7)
		{
			double min = 100;
			student minst;

			for (int i = 0; i < n; i++)
			{
				if (st[i].ukupan_broj_bodova < min)
				{
					min = st[i].ukupan_broj_bodova;
					minst = st[i];
				}

			}

			cout << "\nStudent s najmanjim brojem bodova je: " << minst.ime;
		}

		if (m == 8)
		{
			string nadjiID;
			bool flag = 0;
			cout << "\nunesi ID studenta kojeg trazis: ";
			cin >> nadjiID;

			for (int i = 0; i < n; i++)
			{

				if (nadjiID == st[i].ID)
				{
					cout << "\n" << st[i].ID << ". " << st[i].ime;
					flag = 1;
				}

			}

			if (flag == 0)
				cout << "\nID studenta ne postoji";

		}



		if (m == 9)
		{
			int i, j;
			for (i = 0; i < n - 1; i++)    
				for (j = 0; j < n - i - 1; j++)
					if (st[j].ukupan_broj_bodova > st[j + 1].ukupan_broj_bodova)
					{
						swap(st[j].ukupan_broj_bodova, st[j + 1].ukupan_broj_bodova);
						swap(st[j].ID, st[j + 1].ID);
						swap(st[j].ime, st[j + 1].ime);
						swap(st[j].spol, st[j + 1].spol);
						swap(st[j].ocjena_kviz1, st[j + 1].ocjena_kviz1);
						swap(st[j].ocjena_kviz2, st[j + 1].ocjena_kviz2);
						swap(st[j].ocjena_midterm, st[j + 1].ocjena_midterm);
						swap(st[j].ocjena_final, st[j + 1].ocjena_final);
					}
						
		}

	}

	if (m == 10)
	{
		exit(0);
	}
	}
	
	
	




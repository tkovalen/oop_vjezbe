// zadatak5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

using namespace std;

struct Producent{
	string name;
	string movie;
	int year;
};

void Unesi(vector<Producent> &v){
	Producent pr;

	cout << "\nunesi ime: ";
	cin >> pr.name;
	cout << "\nunesi ime filma: ";
	cin >> pr.movie;
	cout << "\nunesi godinu: ";
	cin >> pr.year;

	v.push_back(pr);
}

void Print( vector<Producent> &v){
	for (unsigned i = 0; i < v.size(); i++)                          
	{
		cout << "\nime: " << v[i].name << "\nfilm: " << v[i].movie << "\nProduct godina: " << v[i].year;
		cout << "\n";
	}
	
}

vector <string> Najzastupljeniji_Producent( vector <Producent> &v){
	int brojac;
	int zastupljenost=0;
	vector <string> producent;
	

	
	for (unsigned i = 0; i < v.size(); i++) {
		brojac = 0;
		for (unsigned j = i+1; j < v.size(); j++) {
			if (v[i].name == v[j].name) {
				brojac++;													
				if (brojac > zastupljenost) {
					zastupljenost = brojac;
					producent.push_back(v[i].name);
				}

				if (brojac == zastupljenost) 
					producent.push_back(v[i].name);	
			}
		}	
	}

	
	sort(producent.begin(), producent.end());
	producent.erase(unique(producent.begin(), producent.end()), producent.end());		
	
	return producent;
	
}


int main()
{
    /*Napisati strukturu �Producent� s atributima �name�, �movie� i �year�. Korisnik upisuje podatke u vektor struktura Producent. 	Napi�si funkciju koja pronalazi najzastupljenijeg producenta (mogu�ce je vi�se najzastupljenijih).*/
	vector<Producent> v;
	vector<string> producent;

	int n = 6;

	while (n != 0) {
		Unesi(v);
		n--;
	}
	
	Print(v);
	producent=Najzastupljeniji_Producent(v);

	cout << "\nNajzastupljeniji producenti: ";
	for (unsigned i = 0; i < producent.size(); i++) {
		cout << producent[i] << " ";
	}

}


// 2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <ctime>
#include <vector>
#include <algorithm>

using namespace std;


void racunalo(vector<int> &n) {


	int izbor;
	srand(time(NULL));
	izbor = (rand() % 3) + 1;

	if (n.size() == 3)
		izbor = 2;
	if (n.size() == 2)
		izbor = 1;

	for (unsigned int i = 0; i < izbor; i++)
		n.pop_back();

	cout << "\nracunalo je izvuklo " << izbor << " sibice, preostali broj sibica: " << n.size();

}

void korisnik(vector<int> &n) {

	int izbor;
	do {

		cout << "\nizvuci sibicu[1-3]: ";
		cin >> izbor;

		if (n.size() == 3) {
			do {
				cout << "\npreostali broj sibica ne moze biti 0, izvuci opet: ";
				cin >> izbor;
			} while (izbor == 3);
		}

		if (n.size() == 2) {
			do {
				cout << "\npreostali broj sibica ne moze biti 0, izvuci opet: ";
				cin >> izbor;
			} while (izbor == 2);
		}

	} while (izbor < 0 || izbor>3);

	for (unsigned int i = 0; i < izbor; i++)
		n.pop_back();

	cout << "\n preostali broj sibica: " << n.size();
}

int main()
{

	vector<int> n(21);
	int m = 5;

	fill(n.begin(), n.end(), 1);

	while (true) {

		racunalo(n);
		if (n.size() == 1) {
			cout << "\nIzgubili ste!";
			break;
		}

		korisnik(n);
		if (n.size() == 1) {
			cout << "\nVi ste pobjedili!";
			break;
		}


	}

}

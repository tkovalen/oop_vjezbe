// zadatak6.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>
#include <string>
#include "Provjera.h"


using namespace std;

	

int main()
{
    /*Korisnik unosi prirodni broj N, nakon �cega unosi N stringova u vector (duljina svakog
stringa mora biti manja od 20 - provjeriti unos, dozvoljeni su samo brojevi i velika slova
engleske abecede). Ukoliko string sadr�zava slova i brojeve, onda ga treba trasnformirati
na na�cin da se svaki podstring od 2 ili vi�se istih znakova zamijeni brojem ponavljanja (AAA = 3A). S druge strane, ukoliko string sadr�zava samo slova onda ga treba
transformirati suprotno prethodno opisanoj transformaciji (2F = FF).*/

	int N;
	
	do {
		cout << "unesi prirodni broj N: ";
		cin >> N;
	} while (N < 1);


	vector <string> v(N);
	

	for ( int i = 0; i < N; i++) {
		
		do {
			cout << "unesi string[manji od 20 znakova]: ";
			cin >> v[i];

			while (!Veliko_slovo(v[i])  && !Broj(v[i]) || Malo_slovo(v[i])) {
				cout << "\ndozvoljeni samo brojevi i velika slova, unesi ponovno: ";
				cin >> v[i];
			}
		} while (v[i].length() >= 20);	
			
	}

	cout << "\nmodificirani string: ";

	for (int i = 0; i < N; i++) {
		
		Modificiraj(v[i]);
	}

}


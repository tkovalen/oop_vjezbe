#ifndef PROVJERA_H
#include "pch.h"
#include <iostream>
#include <vector>
#include <string>

using namespace std;

bool Malo_slovo(string s);
bool Veliko_slovo(string s);
bool Broj(string s);
void Modificiraj(string s);

#endif // !PROVJERA_H



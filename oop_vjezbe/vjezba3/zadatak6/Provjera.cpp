
#include "pch.h"
#include <iostream>
#include <vector>
#include <string>


using namespace std;

bool Malo_slovo(string s) {
	for (unsigned int i = 0; i < s.size(); i++)
	{
		if ((int)(s[i]) >= 97 && (int)(s[i]) <= 122)
			return true;

	}
	return false;
}

bool Veliko_slovo(string s)
{
	for (unsigned int i = 0; i < s.size(); i++)
	{

		if (((int)(s[i]) >= 65 && (int)(s[i]) <= 90) || ((int)(s[i]) >= 47 && (int)(s[i]) <= 57))
			return true;

	}
	return false;
}

bool Broj(string s) {
	for (unsigned int i = 0; i < s.size(); i++)
	{
		if ((int)(s[i]) >= 47 && (int)(s[i]) <= 57)
			return true;

	}
	return false;
}


void Modificiraj(string s) {

	int n;
	string a;
	vector<string> v;
	int brojac = 1;

	for (unsigned int i = 0; i < s.size(); i++) {

		if ((int)(s[i]) >= 47 && (int)(s[i]) <= 57) {

			a = s[i];
			n = atoi(a.c_str());							//int u string
			for (int j = 1; j < n; j++) {
				a = s[i + 1];
				v.push_back(a);

			}

		}

		else {
			a = s[i];
			if (s[i] == s[i + 1]) {
				brojac++;
				a = s[i];
			}

			else {
				string br = to_string(brojac);		//string to int
				if (brojac > 1) {
					v.push_back(br + a);
					brojac = 1;
				}
				else
					v.push_back(a);
			}
		}
	}

	cout << " ";
	for (unsigned int i = 0; i < v.size(); i++) {
		cout << v[i];
	}

}
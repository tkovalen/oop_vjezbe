// zadatak2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>
#include <algorithm>
#include "Header.h"

using namespace std;



vector <int> treci_vektor(vector <int> v1, vector <int> v2)
{
	vector <int> v3;

	for (unsigned int i = 0; i < v1.size(); i++)
	{
		if (!binary_search(v2.begin(), v2.end(), v1.at(i)))
			v3.push_back(v1.at(i));

	}

	return v3;
}


int main()
{
    
	/*Koriste�ci funkcije iz prvog zadatka unijeti dva vektora i formirati novi vektor koji se sastoji od elemenata iz prvog vektora koji nisu u drugom vektoru. Koristiti binary search
funkciju.
Funkcije i njihove prototipe napisati u posebnim datotekama.*/




	int n = 5;
	vector <int> v1;
	vector <int> v2;
	vector <int> v3;

	v1 = novi_vektor(n);
	v2 = novi_vektor(n);

	sort(v1.begin(), v1.end()); //sort vektora
	sort(v2.begin(), v2.end());

	v3 = treci_vektor(v1, v2);


	cout << "\nprvi vektor: ";
	print(v1, n);
	cout << "\ndrugi vektor: ";
	print(v2, n);
	cout << "\nnovi vektor: ";
	print(v3, v3.size());



}


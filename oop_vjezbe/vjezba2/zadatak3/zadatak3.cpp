// zadatak3.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
using namespace std;

typedef struct {
	int logicka_velicina;      // slots used so far
	int fizicka_velicina;  // total available slots
	int *pniz;     // array of integers we're storing
} Vector;


void push_back(int niz[],int n)
{
	for (int i = n; i < n + 1; i++)
	{
		cout << "dodaj novi element na kraj niza: ";
		cin >> niz[i];
	}
		

}

void pop_back(int niz[], int n)
{
	cout<<"\nizbrisan element s kraja niza: ";
	for (int i = 0; i < n - 1; i++)
		cout << " " << niz[i];

}

int vector_size(int niz[], int n)
{
	int brojac = 0;

	for (int i = 0; i < n ; i++)
		brojac++;

	return brojac;
}

void print(int niz[], int n)
{
	for (int i = 0; i < n; i++)
		cout << " "<<niz[i];
}

int& vector_front(int niz[], int n)
{
	
	for (int i = 0; i < 1; i++)
		return niz[i];

}

int& vector_back(int niz[], int n)
{
	for (int i = n - 1; i < n; i++)
		return niz[i];
}


int* vector_new()
{
	int* niz =new int[3]{ 2,1,9 };
	

	return niz;
}

void vector_delete(int niz[])
{
	delete niz;
}

	/*
	Definirati strukturu koja simulira rad vektora. Struktura se sastoji od niza int elemenata, logiícke i fiziícke veliícine niza.
	Fiziícka veliícina je inicijalno init, a kada se ta veliícina napuni vrijednostima, alocira se duplo.
	Napisati implementacije za funkcije vector new, vector delete, vector push back,
	vector pop back, vector front, vector back i vector size.
	*/

	int main()
	{
		Vector vk;

		vk.fizicka_velicina = 3;

		int *niz = new int[vk.fizicka_velicina];

		cout << "unesi niz: ";
		for (int i = 0; i < vk.fizicka_velicina; i++)  //punjenje niza
			cin >> niz[i];
			
	
		int * fizicka_velicina = new int[(vk.fizicka_velicina) * 2]; //dupla alokacija

		vk.pniz = niz; //pointer strukture pokazuje na niz


		push_back(vk.pniz, vk.fizicka_velicina);
		vk.fizicka_velicina++;
		
		print(vk.pniz, vk.fizicka_velicina);
		
		pop_back(vk.pniz, vk.fizicka_velicina);
		vk.fizicka_velicina--;

		cout << "\nvector size: " << vector_size(vk.pniz, vk.fizicka_velicina);

		cout << "\nvector front: " << vector_front(vk.pniz, vk.fizicka_velicina);

		cout << "\nvector back: " << vector_back(vk.pniz, vk.fizicka_velicina);

		cout << "\nvector new: ";
		int *p = vector_new();
		for (int i = 0; i < 3; i++)		//ispis novog niza
			cout << " " << p[i];
		
		vector_delete(p);

		delete fizicka_velicina;
	}

			


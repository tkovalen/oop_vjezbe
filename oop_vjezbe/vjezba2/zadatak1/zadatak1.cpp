// zadatak1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
using namespace std;


int* novi_niz(int niz[])
{

	int brojac = 0;


	for (int i = 0; i < 9; i++)
	{
		if (niz[i - brojac] != i + 1)
		{
			brojac++;
			niz[i]= i + 1;
		}
	}

	return niz;
}


void sort(int niz[], int n)
{
	for (int i = 0; i < n-1; i++)
		for (int j = 0; j < n-1- i; j++)
			if (niz[j] > niz[j + 1])
			{
				swap(niz[j], niz[j + 1]);
			}
}



int main()
{
	/*Napisati funkciju koja pronalazi element koji nedostaje u cjelobrojnom rasponu 1-9.Memoriju za niz alocirati dinami�cki, korisnik popunjava niz, a funkcija prepoznaje i
		vra�ca modificirani sortirani niz s brojem koji nedostaje.U main funkciji ispisati dobiveni
		niz i osloboditi memoriju.*/

	int* niz=new int[9];
	int n = 6;


	for (int i = 0; i < n; i++)
	{
		do
		{
			cout << "unesi " << i + 1 << " clan niza: ";
			cin >> niz[i];
		} while (niz[i] < 1 || niz[i] >9);
		
	}

	//sort
	sort(niz,n);

	int *p = novi_niz(niz);

	for (int i = 0; i < 9; i++)		//ispis novog niza
		cout << " " << p[i];
	

	delete niz;         // oslobodi memoriju.

}


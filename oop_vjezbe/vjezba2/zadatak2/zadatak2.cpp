// zadatak2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
using namespace std;


int *odvoji_niz(int niz[], int n)
{
	int brojac=0;
	for (int i = 0; i < n; i++)
	{
		if (niz[i] % 2 == 0)
		{
			swap(niz[i], niz[brojac]);
			brojac++;
		}

		
	}
	return niz;
}


int main()
{
	                                                            
	/*Napiísite funkciju koja odvaja parne i neparne brojeve u nizu tako da se prvo pojavljuju
parni brojevi, a zatim neparni brojevi. Memoriju za niz alocirati dinamiícki. Primjer ulaza i izlaza:
ulaz: 7 2 4 9 10 11 13 27
izlaz: 10 2 4 9 7 11 13 27*/

	int n = 8;
	int* niz = new int[n] {7, 2, 4, 9, 10, 11, 13, 27};

	int* p = odvoji_niz(niz, n);

	for (int i = 0; i < n; i++)		//ispis novog niza
		cout << " " << p[i];

	delete niz;
}



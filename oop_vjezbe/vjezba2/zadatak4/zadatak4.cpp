// zadatak4.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>
using namespace std;

int* fillarr(int arr[])
{
	for (int i = 0; i < 10; i++)
		arr[i] = i + 3;
	return arr;
}


int main()
{

	/*int  var = 20;   // actual variable declaration.
	int  *ip;        // pointer variable

	ip = &var;       // store address of var in pointer variable

	cout << "Value of var variable: ";
	cout << var << endl;

	// print the address stored in ip pointer variable
	cout << "Address stored in ip variable: ";
	cout << ip << endl;

	// access the value at the address available in pointer
	cout << "Value of *ip variable: ";
	cout << *ip << endl;


	int y[10];
	int *a = fillarr(y);

	for (int i = 0; i < 10; i++)
		cout << " " << a[i];*/


	vector <int> v;

	//punjenje
	v.push_back(3);
	v.push_back(9);
	v.push_back(7);
	v.push_back(12);

	cout << "vector: ";

	for (unsigned int i = 0; i < v.size(); i++)
	{
		cout << v[i] << " ";
	}


	//fizicka velicina int A[15]; then A has physical size 15.

	//logicka velicina total number of occupied slots. It must be less than or equal to the physical size.
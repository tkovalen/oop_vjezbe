#ifndef ORUZJE_H
#define ORUZJE_H



#include "pch.h"
#include <iostream>
#include <vector>
#include "Tocka.h"

using namespace std;

class Oruzje
{

private:

	Tocka t;
	vector <int> v;
	int y = 8;
	int z = 4;


public:

	Tocka Postavi_vrijednost(Tocka t);
	void Shoot();
	void Reload();
	void Print();

};

#endif // !ORUZJE_H;
// zadatak2.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>
#include "Oruzje.h"
#include "Tocka.h"

using namespace std;



int main()
{
	Oruzje o;
	Tocka t;
	
	t.Postavi_vrijednost(1, 5, 0);
	o.Postavi_vrijednost(t);

	for (int i = 0; i < 5; i++) {
		o.Shoot();
		o.Print();
	}
		
	o.Reload();
	o.Print();

	

}

/*Napisati klasu koja predstavlja oruzje. Oruzje ima svoj polozaj u prostoru (jedna tocka
u prostoru), broj metaka koji stanu u jedno punjenje i trenutni broj metaka u punjenju.
Moze pucati(shoot) i ponovo se puniti(reload).*/
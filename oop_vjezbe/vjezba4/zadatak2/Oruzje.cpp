#include "pch.h"
#include "Oruzje.h"

using namespace std;

Tocka Oruzje::Postavi_vrijednost(Tocka t) {

	Reload();
	return t;
	
}

void Oruzje::Shoot() {

	v.pop_back();
}

void Oruzje::Reload() {

	size_t velicina = y - v.size();

	for (size_t i = 0; i < velicina; i++)
		v.push_back(1);

}

void Oruzje::Print() {

	cout << "\ntrenutni broj metaka: " << v.size();
	cout << "\nbroj metaka koji stane u punjenje: " << y;
}
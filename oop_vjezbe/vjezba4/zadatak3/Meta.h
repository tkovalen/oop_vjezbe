#pragma once

#ifndef META_H
#define META_H

#include <iostream>
#include "pch.h"
#include "Tocka.h"

class Meta
{
public:

	double x;
	double y;
	double z;

	Tocka t;


	void Postavivrijednost(Tocka t);
	bool Pogodak();


};

#endif // !META_H
#ifndef ORUZJE_H
#define ORUZJE_H



#include "pch.h"
#include <iostream>
#include <vector>
#include "Tocka.h"

using namespace std;

class Oruzje
{

public:

	vector<int> v;
	int y = 8;
	int z = 4;
	const double x1=5;
	const double y1=8;

	void Shoot();
	void Reload();
	void Print();

};

#endif // !ORUZJE_H;

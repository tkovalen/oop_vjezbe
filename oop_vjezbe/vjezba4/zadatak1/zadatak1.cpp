﻿// zadatak1.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include <vector>
#include <time.h>
#include "Tocka.h"

using namespace std;



//Funkciju za postavljanje pseudorandom vrijednosti ˇclanova (granice intervala su parametri funkcije).



int main()
{
 
	double x1=3;
	double x2 = 5;
	double y1=6;
	double y2 = 10;
	double z1 = 8;
	double z2 = 12;
	double udaljenost = 0;

	Tocka t;

	t.Postavi_vrijednost(x1, y1, z1);

	cout << "ispis tocke: ";
	t.Ispis_tocke();

	t.Random_tocke(1, 20);

	udaljenost = t.Udaljenost2D( x2, y2);
	cout << "\nudaljenost 2D: " << udaljenost;

	udaljenost = t.Udaljenost3D(x2, y2,z2);
	cout << "\nudaljenost 3D: " << udaljenost;

	
	cout << "\nispis random tocke: ";
	t.Ispis_tocke();

}


#include "pch.h"
#include <iostream>
#include <vector>
#include <time.h>
#include "Tocka.h"

using namespace std;


void Tocka::Postavi_vrijednost(double _x, double _y, double _z) {
	x = _x;
	y = _y;
	z = _z;
}

void Tocka::Ispis_tocke() {
	vector<double> v;
		v.push_back(x);
		v.push_back(y);
		v.push_back(z);

		for (unsigned int i = 0; i < v.size(); i++)
			cout << " " << v[i];
}

double Tocka::Udaljenost2D(double _x2, double _y2) {

	double udaljenost = 0;
	double dx = _x2 - x;
	double dy = _y2 - y;

	udaljenost = sqrt((dx*dx) + (dy*dy));

	return udaljenost;
}

double Tocka::Udaljenost3D(double _x2, double _y2, double _z2) {

	double udaljenost = 0;
	double dx = _x2 - x;
	double dy = _y2 - y;
	double dz = _z2 - z;

	udaljenost = sqrt((dx*dx) + (dy*dy) + (dz*dz));

	return udaljenost;
}


void Tocka::Random_tocke(int min, int max) {

	srand(time(NULL));

	x = min + rand() % ((max + 1) - min);
	y = min + rand() % ((max + 1) - min);
}
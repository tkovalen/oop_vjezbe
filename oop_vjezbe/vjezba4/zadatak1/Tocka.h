#pragma once


#ifndef TOCKA_H
#define TOCKA_H

#include "pch.h"
#include <iostream>
#include <vector>
#include <time.h>


using namespace std;

class Tocka {

private:

	double x;
	double y;
	double z;

public:
	

	void Postavi_vrijednost(double _x, double _y, double _z);
	void Ispis_tocke();
	double Udaljenost2D(double _x2, double _y2);
	double Udaljenost3D(double _x2, double _y2, double _z2);
	void Random_tocke(int min, int max);


};


#endif // !H_Tocka;

